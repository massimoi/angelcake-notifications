# Notifications plugin for CakePHP

## Installation
This plugin is installed simply by creating a folder starting with a Capital Letter in the /plugins folders

```bash
cd plugins
git clone https://github.com/impronta48/angelcake-notifications
mv angelcake-notifications Notifications
```

