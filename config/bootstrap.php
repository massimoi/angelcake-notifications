<?php

/**
 * These are the configuration variables for cakephp-notifications
 * Don't forget to configure also 
 * 'EmailTransport' in your app_local.php
 * if you need to use SMTP server different from your local sendmail
 */

use Cake\Core\Configure;

//Configure::write('MailAdmin', ['info@example.com' => 'Example Corp - Customer Inquiry']);
//Configure::write('MailLogo', "example.png");
